﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CityBuilder : MonoBehaviour
{
    [SerializeField] private List<GameObject> templates;
    [SerializeField] private GameObject city;
    [SerializeField] private GameObject cityGrid;

    private readonly List<GameObject> createdBuildings = new List<GameObject>();

    private int count;
    private int totalBuildings = 16; //set based on town size

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C) && createdBuildings.Count < totalBuildings) PlaceBuilding();
    }
    private void PlaceBuilding()
    {
        count = 0;
        var chosenTemplate = templates[Random.Range(0, templates.Count)];
        Create(chosenTemplate);
    }

    private void Create(GameObject objTemplate)
    {
        Vector3 newPos;

        if (createdBuildings.Count == 0)
        { //initial building placement
            newPos = new Vector3(city.transform.position.x + Random.Range(-30, 30), city.transform.position.y, city.transform.position.z + Random.Range(-30, 30));
        }
        else { newPos = GetSpot(objTemplate); }
        //create using the spot chosen
        var createdBuilding = Instantiate(objTemplate, newPos, Quaternion.identity, cityGrid.transform);
        createdBuildings.Add(createdBuilding);
    }

    Vector3 GetSpot(GameObject newB)
    {

        Vector3 placement = new Vector3();
        var goodSpot = false;
        var isClose = 0;
        
        while (count < 20 || (!goodSpot && isClose < 2) || (!goodSpot && isClose > 2 && createdBuildings.Count > 4))
        {
            var existing = createdBuildings[Random.Range(0, createdBuildings.Count)];
            var totalNeededSpace = existing.GetComponent<Renderer>().bounds.extents.magnitude + newB.GetComponent<Renderer>().bounds.extents.magnitude + 3;
            var sideX = Random.Range(0, 2);
            var sideZ = Random.Range(0, 2);
            
            if (createdBuildings.Count < 3)
            {
                existing = createdBuildings[0];
            }

            //decide which side of the selected building to use and calculate spot
            if (sideX == 0 && sideZ == 0)
            { //left
                placement = new Vector3(existing.GetComponent<Renderer>().bounds.center.x - totalNeededSpace, 0, existing.GetComponent<Renderer>().bounds.center.z);
            }
            else if (sideX == 0 && sideZ == 1)
            {//up
                placement = new Vector3(existing.GetComponent<Renderer>().bounds.center.x, 0, existing.GetComponent<Renderer>().bounds.center.z + totalNeededSpace);
            }
            else if (sideX == 1 && sideZ == 0)
            {//right
                placement = new Vector3(existing.GetComponent<Renderer>().bounds.center.x + totalNeededSpace, 0, existing.GetComponent<Renderer>().bounds.center.z);
            }
            else if (sideX == 1 && sideZ == 1)
            {//down
                placement = new Vector3(existing.GetComponent<Renderer>().bounds.center.x, 0, existing.GetComponent<Renderer>().bounds.center.z - totalNeededSpace);
            }

            //verify placement not too close to other buildings
            goodSpot = true;
            foreach (var x in createdBuildings)
            {
                var dist = Vector3.Distance(x.GetComponent<Renderer>().bounds.center, placement) - totalNeededSpace;
                if (dist < 3 || (Vector3.Distance(city.GetComponent<Renderer>().bounds.center, placement) > city.GetComponent<SphereCollider>().radius))
                {
                    goodSpot = false;
                }
                else if (dist == 3)
                {
                    isClose++;
                }
            }
            count++;
        }
        return placement;
    }
}